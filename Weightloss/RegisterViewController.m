//
//  RegisterViewController.m
//  Weightloss
//
//  Created by Kevin Bowler on 03/04/2016.
//  Copyright © 2016 LK. All rights reserved.
//

#import "RegisterViewController.h"
#import <NSString+FontAwesome.h>
#import <QuartzCore/QuartzCore.h>

@interface RegisterViewController ()

@end

@implementation RegisterViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    _lblValName.font = [UIFont fontWithName:kFontAwesomeFamilyName size:20];
    _lblValEmail.font = [UIFont fontWithName:kFontAwesomeFamilyName size:20];
    _lblValPwd.font = [UIFont fontWithName:kFontAwesomeFamilyName size:20];
    _lblValPwdConf.font = [UIFont fontWithName:kFontAwesomeFamilyName size:20];

    _lblValName.textColor = [UIColor grayColor];
    _lblValEmail.textColor = [UIColor grayColor];
    _lblValPwd.textColor = [UIColor grayColor];
    _lblValPwdConf.textColor = [UIColor grayColor];

    _lblValName.layer.borderColor = [UIColor grayColor].CGColor;
    _lblValName.layer.borderWidth = 1.0f;

    _lblValEmail.layer.borderColor = [UIColor grayColor].CGColor;
    _lblValEmail.layer.borderWidth = 1.0f;

    _lblValPwd.layer.borderColor = [UIColor grayColor].CGColor;
    _lblValPwd.layer.borderWidth = 1.0f;

    _lblValPwdConf.layer.borderColor = [UIColor grayColor].CGColor;
    _lblValPwdConf.layer.borderWidth = 1.0f;


    _lblValName.text = [NSString fontAwesomeIconStringForEnum:FAIconUser];
    _lblValEmail.text = [NSString fontAwesomeIconStringForEnum:FAIconEnvelopeAlt];
    _lblValPwd.text = [NSString fontAwesomeIconStringForEnum:FAIconKey];
    _lblValPwdConf.text = [NSString fontAwesomeIconStringForEnum:FAIconKey];

    _vwName.backgroundColor = [UIColor lightGrayColor];
    _vwEmail.backgroundColor = [UIColor lightGrayColor];
    _vwPwd.backgroundColor = [UIColor lightGrayColor];
    _vwPwdConfirm.backgroundColor = [UIColor lightGrayColor];


    for(UITextField *txt in @[_txtName, _txtEmail, _txtPassword, _txtPasswordConfirm]) {
        [txt addTarget:self
                      action:@selector(textFieldDidChange:)
            forControlEvents:UIControlEventEditingChanged];
    }

}

-(void)textFieldDidChange:(UITextField*)txt {
    [self validateFieldsFromTextValue:txt];
}

-(BOOL)validRequired:(NSString*)text {
    if(text.length > 0) {
        return YES;
    }
    return NO;
}

-(BOOL)validateEmailAddress:(NSString*)email {
    BOOL stricterFilter = NO; // Discussion http://blog.logichigh.com/2010/09/02/validating-an-e-mail-address/
    NSString *stricterFilterString = @"^[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}$";
    NSString *laxString = @"^.+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*$";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:email];
}

-(BOOL)validateFieldsFromTextValue:(UITextField*)txt {
    if(txt.tag == 1) {
        BOOL val = [self validRequired:txt.text];
        if(val) {
            [UIView animateWithDuration:0.3f animations:^{
                _lblValName.text = [NSString fontAwesomeIconStringForEnum:FAIconOk];
                _lblValName.textColor = [UIColor colorWithRed:2.0f/255.0f green:135.0f/255.0f blue:13.0f/255.0f alpha:1];
                _vwName.backgroundColor = [UIColor greenColor];
            }];
            return YES;
        }
        [UIView animateWithDuration:0.3f animations:^{
            _lblValName.text = [NSString fontAwesomeIconStringForEnum:FAIconRemove];
            _lblValName.textColor = [UIColor redColor];
            _vwName.backgroundColor = [UIColor colorWithRed:238.0f/255.0f green:180.0f/255.0f blue:180.0f/255.0f alpha:1];
        }];
    }
    if(txt.tag == 2) {
        BOOL val = [self validRequired:txt.text];
        if(val) {
            val = [self validateEmailAddress:txt.text];
            if(val) {
                [UIView animateWithDuration:0.3f animations:^{
                    _lblValEmail.text = [NSString fontAwesomeIconStringForEnum:FAIconOk];
                    _lblValEmail.textColor = [UIColor colorWithRed:2.0f/255.0f green:135.0f/255.0f blue:13.0f/255.0f alpha:1];
                    _vwEmail.backgroundColor = [UIColor greenColor];
                }];
                return YES;
            }

        }
        [UIView animateWithDuration:0.3f animations:^{
            _lblValEmail.text = [NSString fontAwesomeIconStringForEnum:FAIconRemove];
            _lblValEmail.textColor = [UIColor redColor];
            _vwEmail.backgroundColor = [UIColor colorWithRed:238.0f/255.0f green:180.0f/255.0f blue:180.0f/255.0f alpha:1];
        }];
    }
    if(txt.tag == 3) {
        BOOL val = [self validRequired:txt.text];
        if(val) {
            [UIView animateWithDuration:0.3f animations:^{
                _lblValPwd.text = [NSString fontAwesomeIconStringForEnum:FAIconOk];
                _lblValPwd.textColor = [UIColor colorWithRed:2.0f/255.0f green:135.0f/255.0f blue:13.0f/255.0f alpha:1];
                _vwPwd.backgroundColor = [UIColor greenColor];
            }];

            return YES;
        }
        [UIView animateWithDuration:0.3f animations:^{
            _lblValPwd.text = [NSString fontAwesomeIconStringForEnum:FAIconRemove];
            _lblValPwd.textColor = [UIColor redColor];
            _vwPwd.backgroundColor = [UIColor colorWithRed:238.0f/255.0f green:180.0f/255.0f blue:180.0f/255.0f alpha:1];
        }];
    }
    if(txt.tag == 4) {
        if(_txtPassword.text.length > 0) {
            if([_txtPassword.text isEqualToString:_txtPasswordConfirm.text]) {
                [UIView animateWithDuration:0.3f animations:^{
                    _lblValPwdConf.text = [NSString fontAwesomeIconStringForEnum:FAIconOk];
                    _lblValPwdConf.textColor = [UIColor colorWithRed:2.0f/255.0f green:135.0f/255.0f blue:13.0f/255.0f alpha:1];
                    _vwPwdConfirm.backgroundColor = [UIColor greenColor];
                }];
                return YES;
            }
            [UIView animateWithDuration:0.3f animations:^{
                _lblValPwdConf.text = [NSString fontAwesomeIconStringForEnum:FAIconRemove];
                _lblValPwdConf.textColor = [UIColor redColor];
                _vwPwdConfirm.backgroundColor = [UIColor colorWithRed:238.0f/255.0f green:180.0f/255.0f blue:180.0f/255.0f alpha:1];
            }];
        }
    }
    return NO;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)register:(id)sender {
    int validationCounter = 0;

    NSInteger tagCount = 1;
    while(tagCount < 5) {
        UITextField *txt = (UITextField*)[self.view viewWithTag:tagCount];
        BOOL tagValid = [self validateFieldsFromTextValue:txt];
        if(!tagValid) {
            validationCounter++;
        }
        tagCount++;
    }

    if(validationCounter == 0) {
        NSLog(@"All fields are valid");
        // Now, send a request to the server!
        
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)cancel:(id)sender {
    [self dismissViewControllerAnimated:YES completion:^{
        //
    }];;
}


#pragma mark - UITextFieldDelegate methods
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    _currentTextField = textField;
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    [textField resignFirstResponder];
}

@end
