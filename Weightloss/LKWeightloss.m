//
//  LKWeightloss.m
//  Weightloss
//
//  Created by Kevin Bowler on 07/02/2016.
//  Copyright © 2016 LK. All rights reserved.
//

#import "LKWeightloss.h"

@implementation LKWeightloss

+ (LKWeightloss *)sharedManager {
    static LKWeightloss *singletonInstance = nil;
    static dispatch_once_t oncePredicate;
    dispatch_once(&oncePredicate, ^{
        singletonInstance = [[self alloc] init];
    });

    return singletonInstance;
}

+ (void)resetDefaults {
    NSUserDefaults * defs = [NSUserDefaults standardUserDefaults];
    NSDictionary * dict = [defs dictionaryRepresentation];
    for (id key in dict) {
        [defs removeObjectForKey:key];
    }
    [defs synchronize];
}

@end
