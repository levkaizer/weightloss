//
//  LoginViewController.m
//  Weightloss
//
//  Created by Kevin Bowler on 06/02/2016.
//  Copyright © 2016 LK. All rights reserved.
//

#import "LoginViewController.h"
#import "LKAPI.h"
#import "MBProgressHUD.h"


@interface LoginViewController () {
    BOOL showingHUD;
}

@end

@implementation LoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)awakeFromNib {
    NSLog(@"Awoke!");
    showingHUD = NO;
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(hasBeenLoogedIn:) name:@"LKAPILoggedIn" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(hasFailedLogIn:) name:@"LKAPINotLoggedIn" object:nil];
}

-(void)hasBeenLoogedIn:(NSNotification*)notification {
    dispatch_async(dispatch_get_main_queue(), ^{
        if(showingHUD) {
            [MBProgressHUD hideHUDForView:self.view animated:YES];
        }
        [self performSegueWithIdentifier:@"showLoggedIn" sender:self];
    });
}

-(void)hasFailedLogIn:(NSNotification*)notification {
    dispatch_async(dispatch_get_main_queue(), ^{
        if(showingHUD) {
            [MBProgressHUD hideHUDForView:self.view animated:YES];
        }
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Login failed" message:@"You have been logged out due to inactivity." preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
            [alert dismissViewControllerAnimated:YES completion:^{
                //
            }];
        }];
        [alert addAction:ok];
        [self presentViewController:alert animated:YES completion:^{
            //
        }];
    });
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    NSLog(@"Showing View");
    LKAPI *api = [LKAPI apiManager];
    if([api isBeingLoggedIn]) {
        showingHUD = YES;
        NSLog(@"Showing HUD");
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    }
}

-(void)showLoginPopup {
    [self.view setUserInteractionEnabled:NO];
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (IBAction)btnLogin:(id)sender {
    NSString *email = _txtEMail.text;
    NSString *pass = _txtPassword.text;

    if(email.length > 0 && pass.length > 0) {
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        [self.view setUserInteractionEnabled:NO];
        [LKAPI login:@{@"email": email, @"password": pass} withCompleteion:^(NSData *data, NSURLResponse *response, NSError *error) {
            // now we've logged in, get some profile data!
            [LKAPI connectToURL:@"/" withPostData:@{} asHTTPMethod:@"POST" withCompletion:^(NSData *data, NSURLResponse *response, NSError *error) {
                NSError *e = nil;
                NSDictionary *jsonDict = [NSJSONSerialization JSONObjectWithData: data options: NSJSONReadingMutableContainers error: &e];
                NSLog(@"Homepage callback");
                if (!jsonDict) {
                    NSLog(@"Error parsing JSON: %@", e);
                } else {
                    NSDictionary *user = [jsonDict objectForKey:@"user"];
                    NSDictionary *userData = @{
                                                @"email": [user valueForKey:@"email"],
                                                @"id": [user valueForKey:@"id"],
                                                @"name": [user valueForKey:@"name"],
                                                @"created_at": [user valueForKey:@"created_at"],
                                                @"updated_at": [user valueForKey:@"updated_at"]
                                               };
                    [LKAPI saveUserData:userData];

                    dispatch_async(dispatch_get_main_queue(), ^{
                        [MBProgressHUD hideHUDForView:self.view animated:YES];
                        [self.view setUserInteractionEnabled:YES];
                        [self performSegueWithIdentifier:@"showLoggedIn" sender:self];
                    });
                }
            }];
        }];
    }
    else {
        UIAlertController *alert = [UIAlertController
                                      alertControllerWithTitle:@"Error"
                                      message:@"Enter User Credentials"
                                      preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
            [self dismissViewControllerAnimated:YES completion:^{

            }];
        }];
        [alert addAction:ok];

        [self presentViewController:alert animated:YES completion:nil];
    }

}

@end
