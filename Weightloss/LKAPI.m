//
//  LKAPI.m
//  
//
//  Created by Kevin Bowler on 01/02/2016.
//
//

#import "LKAPI.h"
#import "LKWeightloss.h"

@interface LKAPI () {
    BOOL isBeingLoggedIn;
}
@end

@implementation LKAPI

+ (LKAPI *)apiManager {
    static LKAPI *singletonInstance = nil;
    static dispatch_once_t oncePredicate;
    dispatch_once(&oncePredicate, ^{
        singletonInstance = [[self alloc] initWithBaseURL:[NSURL URLWithString:@"https://weightloss.kpbowler.co.uk/api/"]];
    });

    return singletonInstance;
}

- (id)initWithBaseURL:(NSURL *)url {

    self = [super init];

    if (self) {
        isBeingLoggedIn = NO;
        self.baseURL = url;
        NSURLSessionConfiguration *sessionConfig =
        [NSURLSessionConfiguration defaultSessionConfiguration];

        // 3
        NSURLSession *session =
        [NSURLSession sessionWithConfiguration:sessionConfig
                                      delegate:self
                                 delegateQueue:nil];
        self.session = session;

        [self setupSessionConfig];
    }

    return self;
}

- (void) setupSessionConfig {
    NSURLSessionConfiguration *sessionConfig =
    [NSURLSessionConfiguration defaultSessionConfiguration];

    // 1
    sessionConfig.allowsCellularAccess = NO;

    // 2
    //[sessionConfig setHTTPAdditionalHeaders:
     //@{@"Accept": @"application/json"}];

    // 3
    sessionConfig.timeoutIntervalForRequest = 30.0;
    sessionConfig.timeoutIntervalForResource = 60.0;
    sessionConfig.HTTPMaximumConnectionsPerHost = 1;

}

-(NSString*) getCurrentCSRFHeader {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if([defaults objectForKey:@"csrf_token"]) {
        return [defaults valueForKey:@"csrf_token"];
    }
    return @"";
}

-(void) setCSRFHeader:(NSString *)header {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if(header.length > 0) {
        [defaults setObject:header forKey:@"csrf_token"];
        [defaults synchronize];
    }
}

-(void) removeCurrentCSRFHeader {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if([defaults objectForKey:@"csrf_token"]) {
        [defaults removeObjectForKey:@"csrf_token"];
        [defaults synchronize];
    }
}

-(void) setCookie:(NSString*)cookie {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if([cookie isKindOfClass:[NSString class]]  && cookie.length > 0) {
        [defaults setObject:cookie forKey:@"cookie"];
        [defaults synchronize];
    }
}

-(NSString*) getCookie {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if([defaults objectForKey:@"cookie"]) {
        return [defaults valueForKey:@"cookie"];
    }
    return @"";
}

-(void) CSRFToken {
    LKAPI *api = [LKAPI apiManager];
    // make a diversion to the auth
    NSURL *authURL = [api.baseURL URLByAppendingPathComponent:@"token"];
    NSMutableURLRequest *authRequest = [api createRequestwithURL:authURL asHTTPMethod:@"post" withData:@{}];
    NSLog(@"AUTH URL: %@", authURL);
    NSURLSessionDataTask *getDataTask = [api.session dataTaskWithRequest:authRequest completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        NSError *e = nil;
        NSDictionary *jsonDict = [NSJSONSerialization JSONObjectWithData: data options: NSJSONReadingMutableContainers error: &e];

        if (!jsonDict) {
            NSLog(@"Error parsing JSON: %@", e);
        } else {
            // save the csrf
            [api setCSRFHeader:[jsonDict valueForKey:@"csrf"]];
        }

    }];
    [getDataTask resume];
}


#pragma mark - Auth Header access functions
-(NSString*) getCurrentAuthHeader {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if([defaults objectForKey:@"auth_token"]) {
        return [defaults valueForKey:@"auth_token"];
    }
    return @"";
}

-(void) setAuthHeader:(NSString *)header {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if([header isKindOfClass:[NSString class]]  && header.length > 0) {
        [defaults setObject:header forKey:@"auth_token"];
        [defaults synchronize];
    }
}

-(void) removeCurrentAuthHeader {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if([defaults objectForKey:@"auth_token"]) {
        [defaults removeObjectForKey:@"auth_token"];
        [defaults synchronize];
    }
}

- (NSString *) refreshAuthHeaderWithToken:(NSString*)token {
    [self removeCurrentAuthHeader];
    [self setAuthHeader:token];
    return [self getCurrentAuthHeader];

}

- (NSString *)getAuthTokenFromResponse:(NSURLResponse *)response {
    // save the auth token
    NSDictionary* headers = [(NSHTTPURLResponse *)response allHeaderFields];
    //NSLog(@"%@", headers);

    NSString *auth = @"";

    if([headers objectForKey:@"Authorization"]) {
        auth = [headers valueForKey:@"Authorization"];
        NSArray *header = [auth componentsSeparatedByString: @" "];
        if(header.count > 0) {
            auth = header[ header.count-1 ];
        }
    }

    return auth;
}

-(NSString *)getCookieFromResponse:(NSURLResponse *)response {
    NSDictionary* headers = [(NSHTTPURLResponse *)response allHeaderFields];
    NSString *auth = @"";

    if([headers objectForKey:@"Set-Cookie"]) {
        auth = [headers valueForKey:@"Set-Cookie"];
    }

    return auth;
}

-(NSString*)postDataAsString:(NSDictionary*)postData {
    NSMutableString *postString = [[NSMutableString alloc] init];
    int i = 0;
    for(NSString *p in postData) {
        if(i == 0) {
            [postString setString:[NSString stringWithFormat:@"%@=%@&", p, [postData valueForKey:p] ]];
        }
        else {
            [postString appendFormat:@"%@=%@", p, [postData valueForKey:p]];
            if(i < (postData.count-1)) {
                [postString appendString:@"&"];
            }
        }
        i++;
    }
    return [NSString stringWithString:postString];
}

#pragma mark - Connection functions

-(NSMutableURLRequest*) createRequestwithURL:(NSURL*)url asHTTPMethod:(NSString*)method withData:(NSDictionary *)postData {

    NSString *token = [self getCurrentAuthHeader];
    if(token && token.length > 0) {

        NSURLComponents *components = [NSURLComponents new];
        [components setScheme: url.scheme];
        [components setHost:url.host];
        [components setPath:url.path];
        [components setQuery:[NSString stringWithFormat:@"token=%@", token]];

        NSURL *newUrl = [components URL];
        url = newUrl;
    }
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:60.0];
    if( [method caseInsensitiveCompare:@"post"] == NSOrderedSame ) {
        // strings are equal except for possibly case
        [request setHTTPMethod:@"POST"];
    }
    if( [method caseInsensitiveCompare:@"get"] == NSOrderedSame ) {
        // strings are equal except for possibly case
        [request setHTTPMethod:@"GET"];
    }

    NSLog(@"URL: %@", url);

    // do we have a csrf token to add?
    NSString *csrf = [self getCurrentCSRFHeader];
    if(csrf.length > 0) {
        [request addValue:csrf forHTTPHeaderField:@"X-CSRF-TOKEN"];
    }

    // Do we have an auth header?
    if(token && token.length > 0) {
        [request addValue:[NSString stringWithFormat:@"Bearer %@", token] forHTTPHeaderField:@"Authorization"];
        // also add GET var

    }

    // Do we have a cookie?
    NSString *cookie = [self getCookie];
    if([cookie isKindOfClass:[NSString class]] && cookie.length) {
        NSLog(@"cookie: %@", cookie);
        [request addValue:cookie forHTTPHeaderField:@"Set-Cookie"];
    }

    NSString *postString = [self postDataAsString:postData];
    NSData *d = [postString dataUsingEncoding:NSUTF8StringEncoding];
    [request setHTTPBody:d];


    if(postData.count > 0) {
        [request addValue:@"application/x-www-form-urlencoded; charset=UTF-8" forHTTPHeaderField:@"Content-Type"];
    }

    return request;

}

-(void)connectWithUrl:(NSString*)url method:(NSString*)method postData:(NSDictionary*)postData withCompletion:(void (^)(NSData *data, NSURLResponse *response, NSError *error))complete {
    NSURL *urlToLoad = [self.baseURL URLByAppendingPathComponent:url];

    NSMutableURLRequest *request = [self createRequestwithURL:urlToLoad asHTTPMethod:method withData:postData];

    NSURLSessionDataTask *postDataTask = [self.session dataTaskWithRequest:request completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        // call handler...
        //NSLog(@"Pre-callabck...");
        // now we sort out the new token.

        NSError *e = nil;
        NSDictionary *jsonDict = [NSJSONSerialization JSONObjectWithData: data options: NSJSONReadingMutableContainers error: &e];

        NSDictionary* headers = [(NSHTTPURLResponse *)response allHeaderFields];
        NSLog(@"HEADERS: %@", headers);

        if (!jsonDict) {
            NSLog(@"Error parsing JSON: %@", e);
        } else {
            NSLog(@"SET NEW HEADER!");
            NSLog(@"OLD TOKEN: %@", [[LKAPI apiManager] getCurrentAuthHeader]);
            //NSLog(@"%@", jsonDict);

            // get the cookie!
            [self setCookie:[self getCookieFromResponse:response]];

            if((![jsonDict isEqual:[NSNull null]]) && [jsonDict respondsToSelector:@selector(objectForKey:)]  && [jsonDict objectForKey:@"token"]) {
                NSLog(@"TOKEN FROM REQUEST: %@", [jsonDict valueForKey:@"token"]);
                [self setAuthHeader:[jsonDict valueForKey:@"token"] ];
            }
            else {
                NSString *returnedToken = [self getAuthTokenFromResponse:response];
                NSLog(@"TOKEN FROM REQUEST HEADER: %@", returnedToken);
                [self setAuthHeader: returnedToken];
            }
            NSLog(@"NEW TOKEN: %@", [[LKAPI apiManager] getCurrentAuthHeader]);
        }

        complete(data, response, error);


    }];
    NSLog(@"Begin task...");
    [postDataTask resume];
}

+(void) connectToURL:(NSString*)url withPostData:(NSDictionary *)postData asHTTPMethod:(NSString*)method withCompletion:(void (^)(NSData *data, NSURLResponse *response, NSError *error))complete {
    // first thing's first, get the inital token!
    LKAPI *api = [LKAPI apiManager];
    NSLog(@"We have an API!");

    // do we have csrf?
    if([api getCurrentCSRFHeader].length < 1) {
        [api CSRFToken];
        [api connectWithUrl:url method:method postData:postData withCompletion:complete];
    }
    else {
        [api connectWithUrl:url method:method postData:postData withCompletion:complete];
    }

}

#pragma mark - Utility methods
+ (void)login:(NSDictionary*)loginData withCompleteion:(void (^)(NSData *data, NSURLResponse *response, NSError *error))complete {
    [LKAPI connectToURL:@"login" withPostData:loginData asHTTPMethod:@"POST" withCompletion:complete];
}

+ (void)statusWithCompleteion:(void (^)(NSData *data, NSURLResponse *response, NSError *error))complete {
    [LKAPI connectToURL:@"status" withPostData:@{} asHTTPMethod:@"POST" withCompletion:complete];
}

+(void)saveUserData:(NSDictionary*)data {
    if(data.count > 0) {
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        for(id d in data) {
            [defaults setObject:[data valueForKey:d] forKey:d];
        }
        [defaults synchronize];
    }
}

+(void)checkLoginStatus {
    [[LKAPI apiManager] setIsbeingLoggedIn:YES];
    [LKAPI statusWithCompleteion:^(NSData *data, NSURLResponse *response, NSError *error) {
        //;
        [[LKAPI apiManager] setIsbeingLoggedIn:NO];
        NSLog(@"Status callback");
        NSError *e = nil;

        NSString *strData = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
        NSLog(@"%@", strData);

        NSDictionary *jsonDict = [NSJSONSerialization JSONObjectWithData: data options: NSJSONReadingMutableContainers error: &e];
        if (!jsonDict) {
            NSLog(@"Error parsing JSON: %@", e);
        }
        else {
            NSLog(@"%@", jsonDict);
        }

        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
        if([httpResponse statusCode] != 200) {
            // we need to get the token!
            NSLog(@"Token was INvalid");
            [[NSNotificationCenter defaultCenter] postNotificationName:@"LKAPINotLoggedIn" object:nil];
        }
        else {
            // we are good! Log them in!
            NSLog(@"Token was valid, log in!");
            //[self performSegueWithIdentifier:@"showLoggedIn" sender:self];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"LKAPILoggedIn" object:nil];
        }

    }];
}

+(void)getProfileWithCompletion:(void (^)(NSData *data, NSURLResponse *response, NSError *error))complete {
    [LKAPI connectToURL:@"/" withPostData:@{} asHTTPMethod:@"POST" withCompletion:complete];
}

-(BOOL)isBeingLoggedIn {
    return isBeingLoggedIn;
}

-(void)setIsbeingLoggedIn:(BOOL)status {
    isBeingLoggedIn = status;
}

+(void)logout {
    // need to remove all data
    [LKWeightloss resetDefaults];
    NSLog(@"Cleared defaults...");
    [[NSNotificationCenter defaultCenter] postNotificationName:@"LKAPILogout" object:nil];

}

+(void)weightIn:(NSNumber*)weight withCompletion:(void (^)(NSData *data, NSURLResponse *response, NSError *error))complete {
    [LKAPI connectToURL:@"weight" withPostData:@{@"weight": weight} asHTTPMethod:@"POST" withCompletion:complete];

}

@end
