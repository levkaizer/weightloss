//
//  RegisterViewController.h
//  Weightloss
//
//  Created by Kevin Bowler on 03/04/2016.
//  Copyright © 2016 LK. All rights reserved.
//

#import "ViewController.h"

@interface RegisterViewController : ViewController <UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UITextField *txtName;
@property (weak, nonatomic) IBOutlet UITextField *txtEmail;
@property (weak, nonatomic) IBOutlet UITextField *txtPassword;
@property (weak, nonatomic) IBOutlet UITextField *txtPasswordConfirm;

@property (weak, nonatomic) IBOutlet UILabel *lblValName;
@property (weak, nonatomic) IBOutlet UILabel *lblValEmail;
@property (weak, nonatomic) IBOutlet UILabel *lblValPwd;
@property (weak, nonatomic) IBOutlet UILabel *lblValPwdConf;
@property (weak, nonatomic) IBOutlet UIView *vwName;
@property (weak, nonatomic) IBOutlet UIView *vwEmail;
@property (weak, nonatomic) IBOutlet UIView *vwPwd;
@property (weak, nonatomic) IBOutlet UIView *vwPwdConfirm;

@property (strong, nonatomic) UITextField *currentTextField;

@end
