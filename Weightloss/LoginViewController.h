//
//  LoginViewController.h
//  Weightloss
//
//  Created by Kevin Bowler on 06/02/2016.
//  Copyright © 2016 LK. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LoginViewController : UIViewController
@property (weak, nonatomic) IBOutlet UITextField *txtEMail;
@property (weak, nonatomic) IBOutlet UITextField *txtPassword;
@property (weak, nonatomic) IBOutlet UIButton *btnLogin;

@end
