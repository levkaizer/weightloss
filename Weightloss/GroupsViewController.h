//
//  GroupsViewController.h
//  Weightloss
//
//  Created by Kevin Bowler on 11/02/2016.
//  Copyright © 2016 LK. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GroupsViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, strong) NSArray *groups;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic, assign) id delegate;
@property (nonatomic, readwrite) NSUInteger defaultGroup;

@end
