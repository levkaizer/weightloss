//
//  GroupsViewController.m
//  Weightloss
//
//  Created by Kevin Bowler on 11/02/2016.
//  Copyright © 2016 LK. All rights reserved.
//

#import "GroupsViewController.h"
#import "LKAPI.h"
#import "MBProgressHUD.h"
#import "SocialNetworkViewController.h"
#import "LKUser.h"

@interface GroupsViewController () {
    NSIndexPath *previousRow;
}

@end

@implementation GroupsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated {
    [self getGroups:YES];
    // now get the dfault group, see if it exists
}

-(void)getGroups:(BOOL)animated {
    if(animated) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        });
    }
    [self.view setUserInteractionEnabled:NO];
    [LKAPI connectToURL:@"groups" withPostData:@{} asHTTPMethod:@"POST" withCompletion:^(NSData *data, NSURLResponse *response, NSError *error) {
        NSError *e;
        NSDictionary *jsonDict = [NSJSONSerialization JSONObjectWithData: data options: NSJSONReadingMutableContainers error: &e];
        if (!jsonDict) {
            NSLog(@"Error parsing JSON: %@", e);
        }
        else {
            if([[jsonDict valueForKey:@"error"] boolValue]) {

            }
            else {
                _groups = [NSArray arrayWithArray:[jsonDict objectForKey:@"groups"]];
                if(![LKUser compareGroups:_groups]) {
                    NSLog(@"%d", [LKUser compareGroups:_groups]);
                    [LKUser saveGroups:_groups];
                }
            }
            [self.view setUserInteractionEnabled:YES];
            NSUInteger defaultGroup = [LKUser defaultGroup];
            for(id g in _groups) {
                NSUInteger gid = [[g valueForKey:@"id"] unsignedIntegerValue];
                if(gid == defaultGroup) {
                    // we have a defaut group!
                    _defaultGroup = defaultGroup;
                }
            }
            dispatch_async(dispatch_get_main_queue(), ^{
                [_tableView reloadData];
                if(animated) {
                    [MBProgressHUD hideHUDForView:self.view animated:YES];
                }
            });
        }
    }];
}

- (IBAction)addGroup:(id)sender {
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Group name" message:@"Enter the name of your group:" preferredStyle:UIAlertControllerStyleAlert];

    UIAlertAction *ok = [UIAlertAction actionWithTitle:@"Add" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        UITextField *text = alert.textFields.firstObject;

        if(text.text.length > 0) {
            //
            [LKAPI connectToURL:@"add-group" withPostData:@{@"group":text.text} asHTTPMethod:@"POST" withCompletion:^(NSData *data, NSURLResponse *response, NSError *error) {
                //
                NSError *e;
                NSDictionary *jsonDict = [NSJSONSerialization JSONObjectWithData: data options: NSJSONReadingMutableContainers error: &e];
                if (!jsonDict) {
                    NSLog(@"Error parsing JSON: %@", e);
                }
                else {
                    NSLog(@"%@", jsonDict);
                }
                if([[jsonDict valueForKey:@"error"] boolValue]) {
                    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Error" message:@"You alreay have a group with this name." preferredStyle:UIAlertControllerStyleAlert];

                    UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
                        //
                        [alert dismissViewControllerAnimated:YES completion:^{
                            //
                        }];
                    }];

                    [alert addAction:ok];

                    dispatch_async(dispatch_get_main_queue(), ^{
                        [self presentViewController:alert animated:YES completion:^{
                            //
                        }];
                    });
                }
                else {
                    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Success" message:@"Your group has been created!." preferredStyle:UIAlertControllerStyleAlert];

                    UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
                        //
                        [self getGroups:NO];
                        [alert dismissViewControllerAnimated:YES completion:^{
                            //
                        }];
                    }];

                    [alert addAction:ok];

                    dispatch_async(dispatch_get_main_queue(), ^{
                        [self presentViewController:alert animated:YES completion:^{
                            //
                        }];
                    });
                }
            }];
        }

        [alert dismissViewControllerAnimated:YES completion:^{
            //
        }];
    }];

    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        [alert dismissViewControllerAnimated:YES completion:^{
            //
        }];
    }];

    [alert addTextFieldWithConfigurationHandler:^(UITextField * _Nonnull textField) {
        //
    }];

    [alert addAction:ok];
    [alert addAction:cancel];

    [self presentViewController:alert animated:YES completion:^{
        //
    }];
}

- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *MyIdentifier = @"groupCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:MyIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault  reuseIdentifier:MyIdentifier];
    }
    cell.textLabel.text = [_groups[indexPath.row] valueForKey:@"name"];
    cell.accessoryType = UITableViewCellAccessoryNone;
    if( [[_groups[indexPath.row] valueForKey:@"id"] unsignedIntegerValue] == _defaultGroup ) {
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    }
    return cell;
}

- (NSInteger)tableView:(UITableView *)tableView
 numberOfRowsInSection:(NSInteger)section {
    return _groups.count;
}

- (void)tableView:(UITableView *)tableView
didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    SocialNetworkViewController *svc = self.delegate;
    svc.selectedGroup = [[_groups[indexPath.row] valueForKey:@"id"]  unsignedIntegerValue];
    svc.returnedFromGroups = YES;
    NSDictionary *g = _groups[indexPath.row];
    NSNumber *gid = [g valueForKey:@"id"];
    NSUInteger defaultGid = [gid unsignedIntegerValue];

    [LKUser setDefaultGroup:&defaultGid];
    _defaultGroup = [LKUser defaultGroup];
    [_tableView reloadData];
    [self dismissViewControllerAnimated:YES completion:^{
        //
    }];
}
- (IBAction)cancel:(id)sender {
    [self.navigationController dismissViewControllerAnimated:YES completion:^{
        //
    }];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
