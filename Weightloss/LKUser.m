//
//  LKUser.m
//  Weightloss
//
//  Created by Kevin Bowler on 06/02/2016.
//  Copyright © 2016 LK. All rights reserved.
//

#import "LKUser.h"

@implementation LKUser

+ (LKUser *)sharedUser {
    static LKUser *singletonInstance = nil;
    static dispatch_once_t oncePredicate;
    dispatch_once(&oncePredicate, ^{
        singletonInstance = [[self alloc] init];
    });

    return singletonInstance;
}

+(NSDictionary *)getProfileData {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];

    NSDictionary *user = @{
                           @"email": [defaults valueForKey:@"email"],
                           @"name": [defaults valueForKey:@"name"],
                           @"id": [defaults valueForKey:@"id"],
                           @"created_at": [defaults valueForKey:@"created_at"],
                           @"updated_at": [defaults valueForKey:@"updated_at"]
                           };

    return user;
}

+(void)saveGroups:(NSArray*)groups {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:groups forKey:@"groups"];
    [defaults synchronize];
}

+(NSArray*)groups {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if([defaults objectForKey:@"groups"]) {
        NSArray *groups = [NSArray arrayWithArray:[defaults objectForKey:@"groups"] ];
        return groups;
    }
    return [NSArray array];
}

+(BOOL) compareGroups:(NSArray*)groups {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSArray *savedGroups = [NSArray arrayWithArray:[defaults objectForKey:@"groups"]];
    return [groups hash] == [savedGroups hash];
}

+(BOOL)hasDefaultGroup {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if([defaults objectForKey:@"default_group"]) {
        return YES;
    }
    return NO;
}

+(void)setDefaultGroup:(NSUInteger*)groupId {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSNumber *group = [NSNumber numberWithUnsignedInteger:*groupId];
    [defaults setValue:group forKey:@"default_group"];
}

+(NSUInteger)defaultGroup {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if([defaults objectForKey:@"default_group"]) {
        NSNumber *group = [defaults valueForKey:@"default_group"];

        return [group unsignedIntegerValue];
    }
    return [[NSNumber numberWithUnsignedChar:0] unsignedIntegerValue];
}

+(void)setHistory:(NSArray*)history {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:history forKey:@"history"];
    [defaults synchronize];
}

+(NSArray*)history {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if([defaults objectForKey:@"history"]) {
        NSArray *history = [NSArray arrayWithArray:[defaults objectForKey:@"history"] ];
        return history;
    }
    return [NSArray array];
}

@end
