//
//  ProfileViewController.m
//  Weightloss
//
//  Created by Kevin Bowler on 06/02/2016.
//  Copyright © 2016 LK. All rights reserved.
//

#import "ProfileViewController.h"
#import "LKAPI.h"
#import "LKUser.h"
#import "ActionSheetPicker.h"

CGFloat const kJBLineChartViewControllerChartSolidLineWidth = 3.0f;

@interface ProfileViewController () {
    NSDictionary *user;
    NSArray *history;
    BOOL showingOnBoard;
}

@end

@implementation ProfileViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    _chartView.delegate = self;
    _chartView.dataSource = self;
    showingOnBoard = NO;
    history = [NSArray array];

    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(checkLoginStatusUponReturningFromBackground:)
                                                 name:UIApplicationWillEnterForegroundNotification
                                               object:nil];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];

    user = [LKUser getProfileData];

    [UIView animateWithDuration:0.2f animations:^{
        [_waitingView setHidden:NO];
    }];

    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(hasLoggedOut:)
                                                 name:@"LKAPILogout"
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(hasLoggedOut:)
                                                 name:@"LKAPINotLoggedIn"
                                               object:nil];

    [LKAPI getProfileWithCompletion:^(NSData *data, NSURLResponse *response, NSError *error) {
        NSError *e = nil;
        NSDictionary *jsonDict = [NSJSONSerialization JSONObjectWithData: data options: NSJSONReadingMutableContainers error: &e];
        NSLog(@"Profile callback");
        if (!jsonDict) {
            NSLog(@"Error parsing JSON: %@", e);
        } else {
            NSLog(@"%@", jsonDict);
            NSDictionary *userDict = [jsonDict objectForKey:@"user"];
            // get the weight
            if([userDict objectForKey:@"weight"] && [[userDict objectForKey:@"weight"] isKindOfClass:[NSDictionary class]]) {
                NSDictionary *weightDict = [userDict objectForKey:@"weight"];
                if([weightDict objectForKey:@"weight"]) {
                    NSNumber *weight = [weightDict valueForKey:@"weight"];
                    [LKUser sharedUser].weight = weight;
                    dispatch_async(dispatch_get_main_queue(), ^{
                        _lblWeight.text = [NSString stringWithFormat:@"%0.2f", [weight floatValue]];
                    });
                }
            }

            // set the default group only if we dont have one (basically on the first run)
            if(![LKUser hasDefaultGroup]) {
                NSUInteger dg = [[userDict valueForKey:@"default_group"] unsignedIntegerValue];
                [LKUser setDefaultGroup:&dg];
            }
        }
    }];

    // Now get history
    [self getHistory];
    if(!showingOnBoard) {
        [self showOnboard];
    }


}

-(void)showOnboard {
    NSMutableAttributedString *targetText = [[NSMutableAttributedString alloc] initWithString:@"Enter the weight you would like to reach."];
    [targetText addAttribute:NSFontAttributeName
                  value:[UIFont systemFontOfSize:13.0]
                  range:NSMakeRange(0, targetText.length)];

    showingOnBoard = YES;
    SFDraggableDialogView *dialogView = [[[NSBundle mainBundle] loadNibNamed:@"SFDraggableDialogView" owner:self options:nil] firstObject];
    dialogView.frame = self.view.bounds;
    dialogView.photo = [UIImage imageNamed:@"Target"];
    dialogView.delegate = self;
    dialogView.titleText = [[NSMutableAttributedString alloc] initWithString:@"Set your Target"];
    dialogView.messageText = targetText;

    dialogView.firstBtnText = [@"Get Started" uppercaseString];
    dialogView.dialogBackgroundColor = [UIColor whiteColor];
    dialogView.cornerRadius = 8.0;
    dialogView.backgroundShadowOpacity = 0.2;
    dialogView.hideCloseButton = true;
    dialogView.showSecondBtn = false;
    dialogView.contentViewType = SFContentViewTypeDefault;
    dialogView.firstBtnBackgroundColor = [UIColor colorWithRed:0.230 green:0.777 blue:0.316 alpha:1.000];
    [dialogView createBlurBackgroundWithImage:[self pb_takeSnapshot] tintColor:[[UIColor blackColor] colorWithAlphaComponent:0.65] blurRadius:100.0];

    [self.view addSubview:dialogView];
}

- (UIImage *)pb_takeSnapshot {
    UIGraphicsBeginImageContextWithOptions(self.view.bounds.size, NO, [UIScreen mainScreen].scale);

    [self.view drawViewHierarchyInRect:self.view.bounds afterScreenUpdates:YES];

    // old style [self.layer renderInContext:UIGraphicsGetCurrentContext()];

    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return image;
}

-(void)getHistory {
    if(_waitingView.hidden) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [UIView animateWithDuration:0.2f animations:^{
                [_waitingView setHidden:NO];
            }];
        });
    }
    [LKAPI connectToURL:@"history" withPostData:@{} asHTTPMethod:@"POST" withCompletion:^(NSData *data, NSURLResponse *response, NSError *error) {
        NSError *e = nil;
        NSDictionary *jsonDict = [NSJSONSerialization JSONObjectWithData: data options: NSJSONReadingMutableContainers error: &e];
        if (!jsonDict) {
            NSLog(@"Error parsing JSON: %@", e);
        } else {
            NSLog(@"HISTORY!, %@", jsonDict);
            history = [jsonDict objectForKey:@"history"];
            dispatch_async(dispatch_get_main_queue(), ^{
                [UIView animateWithDuration:0.2f animations:^{
                    [_waitingView setHidden:YES];
                }];
                [_chartView reloadData];
            });
        }
    }];
}

-(void) viewDidDisappear:(BOOL)animated {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"LKAPILogout" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"LKAPINotLoggedIn" object:nil];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)btnSettings:(id)sender {
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Settings" message:@"Select an action" preferredStyle:UIAlertControllerStyleActionSheet];

    UIAlertAction *logout = [UIAlertAction actionWithTitle:@"Log out" style:UIAlertActionStyleDestructive handler:^(UIAlertAction * _Nonnull action) {
        //
        [LKAPI logout];
        [alert dismissViewControllerAnimated:YES completion:^{
            //
        }];
    }];

    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel     handler:^(UIAlertAction * _Nonnull action) {
        //
        [alert dismissViewControllerAnimated:YES completion:^{
        }];
    }];

    [alert addAction:logout];
    [alert addAction:cancel];

    [self presentViewController:alert animated:YES completion:^{
        //
    }];
}

- (IBAction)btnWeighIin:(id)sender {
    //[self performSegueWithIdentifier:@"showWeighIn" sender:self];
    [ActionSheetDistancePicker showPickerWithTitle:@"Select weight" bigUnitString:@"." bigUnitMax:330 selectedBigUnit:self.selectedBigUnit smallUnitString:@"kg" smallUnitMax:99 selectedSmallUnit:self.selectedSmallUnit target:self action:@selector(measurementWasSelectedWithBigUnit:smallUnit:element:) origin:sender];
}

- (void)measurementWasSelectedWithBigUnit:(NSNumber *)bigUnit smallUnit:(NSNumber *)smallUnit element:(id)element {
    self.selectedBigUnit = [bigUnit intValue];
    self.selectedSmallUnit = [smallUnit intValue];
    NSLog(@"Weight: %i kg and %i g", [bigUnit intValue], [smallUnit intValue]);
    CGFloat weight = (CGFloat)[bigUnit intValue] + ((CGFloat)[smallUnit intValue] / 100);
    NSLog(@"%.02f", weight);
    NSNumber *WeightToSubmit = [NSNumber numberWithFloat:weight];
    [LKAPI weightIn:WeightToSubmit withCompletion:^(NSData *data, NSURLResponse *response, NSError *error) {
        //now we update the weight above...
        [LKUser sharedUser].weight = WeightToSubmit;
        dispatch_async(dispatch_get_main_queue(), ^{
            _lblWeight.text = [NSString stringWithFormat:@"%0.2f", [WeightToSubmit  floatValue]];
            
        });
        [self getHistory];
        
    }];
}

#pragma mark - JBChartDataSource methods

- (NSUInteger)numberOfLinesInLineChartView:(JBLineChartView *)lineChartView
{
    return 1; // number of lines in chart
}

- (NSUInteger)lineChartView:(JBLineChartView *)lineChartView numberOfVerticalValuesAtLineIndex:(NSUInteger)lineIndex
{
    return history.count; // number of values for a line
}

- (BOOL)shouldExtendSelectionViewIntoHeaderPaddingForChartView:(JBChartView *)chartView
{
    return YES;
}

#pragma mrk - SFDraggableDialogViewDelegate methods
- (void)draggableDialogViewDismissed:(SFDraggableDialogView *)dialogView {
    showingOnBoard = NO;
}

#pragma mark - JBLineChartViewDelegate methods

- (CGFloat)lineChartView:(JBLineChartView *)lineChartView verticalValueForHorizontalIndex:(NSUInteger)horizontalIndex atLineIndex:(NSUInteger)lineIndex
{
    return [[[history objectAtIndex:horizontalIndex] valueForKey:@"weight"] floatValue]; // y-position (y-axis) of point at horizontalIndex (x-axis)
}

- (UIColor *)lineChartView:(JBLineChartView *)lineChartView colorForDotAtHorizontalIndex:(NSUInteger)horizontalIndex atLineIndex:(NSUInteger)lineIndex
{
    return kJBColorLineChartDefaultSolidLineColor;
}

- (CGFloat)lineChartView:(JBLineChartView *)lineChartView widthForLineAtLineIndex:(NSUInteger)lineIndex
{
    return kJBLineChartViewControllerChartSolidLineWidth;
}

-(void)hasLoggedOut:(NSNotification*)notification {
    NSLog(@"ROll back view from notification");
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.tabBarController.navigationController popViewControllerAnimated:YES];
    });
}

-(void)checkLoginStatusUponReturningFromBackground:(NSNotification*)notification {
    UINavigationController *nav = (UINavigationController*) self.navigationController;
    if(nav.presentedViewController == self) {
        [LKAPI checkLoginStatus];
    }
}

@end
