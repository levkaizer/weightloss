//
//  LKWeightloss.h
//  Weightloss
//
//  Created by Kevin Bowler on 07/02/2016.
//  Copyright © 2016 LK. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LKWeightloss : NSObject

+ (LKWeightloss *)sharedManager;

+ (void)resetDefaults;

@end
