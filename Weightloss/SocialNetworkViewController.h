//
//  SocialNetworkViewController.h
//  Weightloss
//
//  Created by Kevin Bowler on 10/02/2016.
//  Copyright © 2016 LK. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Contacts/Contacts.h>
#import <ContactsUI/ContactsUI.h>

@interface SocialNetworkViewController : UIViewController <CNContactPickerDelegate, UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, readwrite) NSUInteger selectedGroup;
@property (nonatomic) BOOL returnedFromGroups;
@property (weak, nonatomic) IBOutlet UIView *vwNoGroups;
@property (weak, nonatomic) IBOutlet UIView *vwNoPosts;
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end
