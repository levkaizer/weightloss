//
//  ProfileViewController.h
//  Weightloss
//
//  Created by Kevin Bowler on 06/02/2016.
//  Copyright © 2016 LK. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JBChartView.h"
#import "JBLineChartView.h"
#import <SFDraggableDialogView.h>

#define kJBColorLineChartDefaultSolidLineColor [UIColor colorWithWhite:1.0 alpha:0.5]

@interface ProfileViewController : UIViewController <JBLineChartViewDataSource, JBLineChartViewDelegate, SFDraggableDialogViewDelegate>
@property (weak, nonatomic) IBOutlet UILabel *lblEmail;
- (IBAction)btnSettings:(id)sender;
- (IBAction)btnWeighIin:(id)sender;
@property (nonatomic, assign) NSInteger selectedBigUnit;
@property (nonatomic, assign) NSInteger selectedSmallUnit;
@property (weak, nonatomic) IBOutlet UILabel *lblWeight;
@property (weak, nonatomic) IBOutlet JBLineChartView *chartView;

@property (weak, nonatomic) IBOutlet UIView *waitingView;

@end
