//
//  WeighInViewController.h
//  Weightloss
//
//  Created by Kevin Bowler on 09/02/2016.
//  Copyright © 2016 LK. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WeighInViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIBarButtonItem *btnCancel;


- (IBAction)cancel:(id)sender;

@end
