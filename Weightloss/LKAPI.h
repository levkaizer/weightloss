//
//  LKAPI.h
//  
//
//  Created by Kevin Bowler on 01/02/2016.
//
//

#import <Foundation/Foundation.h>

@interface LKAPI : NSObject <NSURLSessionDataDelegate>

+ (LKAPI *)apiManager;

@property (nonatomic, strong) NSURL *baseURL;
@property (nonatomic, strong) NSURLSession *session;
@property (nonatomic, strong) NSURLSessionConfiguration *sessionConfig;

- (void) setupSessionConfig;
-(NSMutableURLRequest*) createRequestwithURL:(NSURL*)url asHTTPMethod:(NSString*)method withData:(NSDictionary *)postData;

-(void) CSRFToken;

-(NSString*) getCurrentAuthHeader;

+(void) connectToURL:(NSString*)url withPostData:(NSDictionary *)postData asHTTPMethod:(NSString*)method withCompletion:(void (^)(NSData *data, NSURLResponse *response, NSError *error))complete;

+ (void)login:(NSDictionary*)loginData withCompleteion:(void (^)(NSData *data, NSURLResponse *response, NSError *error))complete;
+ (void)statusWithCompleteion:(void (^)(NSData *data, NSURLResponse *response, NSError *error))complete;

+(void)getProfileWithCompletion:(void (^)(NSData *data, NSURLResponse *response, NSError *error))complete;

+(void)saveUserData:(NSDictionary*)data;

+(void)checkLoginStatus;

+(void)weightIn:(NSNumber*)weight withCompletion:(void (^)(NSData *data, NSURLResponse *response, NSError *error))complete;

-(BOOL)isBeingLoggedIn;
-(void)setIsbeingLoggedIn:(BOOL)status;

+(void)logout;

@end
