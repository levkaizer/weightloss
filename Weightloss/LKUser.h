//
//  LKUser.h
//  Weightloss
//
//  Created by Kevin Bowler on 06/02/2016.
//  Copyright © 2016 LK. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LKUser : NSObject

@property (nonatomic, strong) NSNumber *weight;

+ (LKUser *)sharedUser;

+(NSDictionary *)getProfileData;

+(void)saveGroups:(NSArray*)groups;

+(NSArray*)groups;

+(BOOL) compareGroups:(NSArray*)groups;

+(BOOL)hasDefaultGroup;

+(void)setDefaultGroup:(NSUInteger*)groupId;

+(NSUInteger)defaultGroup;

+(void)setHistory:(NSArray*)history;

+(NSArray*)history;

@end
