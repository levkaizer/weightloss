//
//  SocialNetworkViewController.m
//  Weightloss
//
//  Created by Kevin Bowler on 10/02/2016.
//  Copyright © 2016 LK. All rights reserved.
//

#import "SocialNetworkViewController.h"
#import "GroupsViewController.h"
#import "LKUser.h"
#import "LKAPI.h"

@interface SocialNetworkViewController () {
    NSMutableArray *contacts;
    NSArray *groups;
    NSArray *posts;
}

@end

@implementation SocialNetworkViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    contacts = [NSMutableArray array];
    groups = [NSArray array];
    posts = [NSArray array];
    _selectedGroup = 0;
    _returnedFromGroups = NO;

    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(checkLoginStatusUponReturningFromBackground:)
                                                 name:UIApplicationWillEnterForegroundNotification
                                               object:nil];
}

-(void)viewWillAppear:(BOOL)animated {
    // load data fmor cache if we havent chosen a group
    if(!_returnedFromGroups) {
        _selectedGroup = [LKUser defaultGroup];
    }

    groups = [LKUser groups];

    if (groups.count > 0) {
        [_vwNoGroups setHidden:YES];
        [_vwNoPosts setHidden:NO];
    }
    if(_returnedFromGroups) {
        _returnedFromGroups = NO;
    }
    //NSNumber *gid = [NSNumber numberWithUnsignedInteger:_selectedGroup];
    [LKAPI connectToURL:@"posts" withPostData:@{} asHTTPMethod:@"POST" withCompletion:^(NSData *data, NSURLResponse *response, NSError *error) {
        NSError *e = nil;
        NSDictionary *jsonDict = [NSJSONSerialization JSONObjectWithData: data options: NSJSONReadingMutableContainers error: &e];
        
        if (!jsonDict) {
            NSLog(@"Error parsing JSON: %@", e);
        } else {
            posts = [jsonDict valueForKey:@"posts"];
            dispatch_async(dispatch_get_main_queue(), ^{
                [_tableView reloadData];
            });
        }
        
    }];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)selectContact:(id)sender {
    [self getContacts];
}

-(void)getContacts {
    if([CNContactStore class]) {
        CNContactStore *addressBook = [[CNContactStore alloc] init];
        [addressBook requestAccessForEntityType:CNEntityTypeContacts completionHandler:^(BOOL granted, NSError * _Nullable error) {
            CNContactPickerViewController *cpvc = [[CNContactPickerViewController alloc] init];
            cpvc.delegate = self;
            [self presentViewController:cpvc animated:YES completion:^{
                //
            }];

        }];
    }
}

- (IBAction)getGroups:(id)sender {
    [self performSegueWithIdentifier:@"showGroups" sender:self];
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if([segue.identifier isEqualToString:@"showGroups"]) {
        UINavigationController *nvc = (UINavigationController*)segue.destinationViewController;
        GroupsViewController *gvc = (GroupsViewController*)nvc.topViewController;
        gvc.delegate = self;
    }
}

#pragma mark - UITableviewDelegate methods
- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *MyIdentifier = @"postCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:MyIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault  reuseIdentifier:MyIdentifier];
    }
    cell.textLabel.text = [posts[indexPath.row] valueForKey:@"content"];
    return cell;
}

- (NSInteger)tableView:(UITableView *)tableView
 numberOfRowsInSection:(NSInteger)section {
    return posts.count;
}

-(void)checkLoginStatusUponReturningFromBackground:(NSNotification*)notification {
    UINavigationController *nav = (UINavigationController*) self.navigationController;
    if(nav.presentedViewController == self) {
        [LKAPI checkLoginStatus];
    }
}


#pragma mark - CNContactPickerDelegete Methods


- (void)contactPickerDidCancel:(CNContactPickerViewController *)picker {
    
}


- (void)contactPicker:(CNContactPickerViewController *)picker
didSelectContactProperty:(CNContactProperty *)contactProperty {
    NSString *chosenEmail = [NSString stringWithFormat:@"%@", contactProperty.value];

    // now ping it to the server...
    
}

@end
